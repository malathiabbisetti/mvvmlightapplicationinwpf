using DataGridApplication.Model;
using GalaSoft.MvvmLight;
using System.Collections.Generic;
using System.Windows.Data;

namespace DataGridApplication.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public string Title { get; set; }

        public List<Employee> Employees { get; } = new List<Employee>();

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            //var mainWindow = new MainWindow();
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                Title = "DataGrid Example(Design Mode)";
            }
            else
            {
                // Code runs "for real"
                Title = "DataGrid Example";
            }

            //Employees.Add(new Employee(1, "EMP001", "Malathi", "DotNet"));
            //Employees.Add(new Employee(2, "EMP002", "Mounika", "DotNet"));
            //Employees.Add(new Employee(3, "EMP003", "Shanmi", "Java"));
            //Employees.Add(new Employee(4, "EMP004", "Jethin", "Java"));

            //ListCollectionView collection = new ListCollectionView(Employees);
            //collection.GroupDescriptions.Add(new PropertyGroupDescription("Team"));
            //mainWindow.grdEmployee.ItemsSource = collection;
            
        }
    }
}