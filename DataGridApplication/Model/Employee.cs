﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataGridApplication.Model
{
    public class Employee
    {
        public int Id { get; set; }

        public string EmployeeId { get; set; }

        public string Name { get; set; }

        public string Team { get; set; }

       
    }
}
