﻿using DataGridApplication.Model;
using DataGridApplication.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DataGridApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Employee> Employees { get; }

        public MainWindow()
        {

            InitializeComponent();

            Employees = new List<Employee>()
            {
                 new Employee(){Id = 1, EmployeeId = "EMP001", Name = "Malathi", Team = "DotNet" },
                 new Employee(){Id = 2,  EmployeeId = "EMP002", Name = "Mounika", Team ="DotNet" },
                 new Employee(){Id = 3, EmployeeId = "EMP003", Name = "Shanmi",  Team = "Java" },
                 new Employee(){Id = 4, EmployeeId = "EMP004", Name = "Jethin",  Team = "Java" }

            };


            ListCollectionView collection = new ListCollectionView(Employees);
            collection.GroupDescriptions.Add(new PropertyGroupDescription("Team"));
            grdEmployee.ItemsSource = collection;
        }
    }
}
