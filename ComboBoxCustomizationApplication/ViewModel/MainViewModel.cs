using ComboBoxCustomizationApplication.Model;
using GalaSoft.MvvmLight;
using System.Collections.Generic;

namespace ComboBoxCustomizationApplication.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public string Title { get; set; }
        public List<Employee> employees { get; }
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                Title = "ComboBox Customization Application(Design Mode)";
            }
            else
            {
                // Code runs "for real"
                Title = "ComboBox Customization Application";
            }

            employees = new List<Employee>()
            {
                new Employee(){Id = 1, Name = "Malathi", Address = "Mogaltur" },
                new Employee(){Id = 2, Name = "Mounika", Address = "Mogaltur"},
                new Employee(){Id = 3, Name = "Shanmi", Address = "Palakol"},
                new Employee(){Id = 4, Name = "Yasaswi", Address = "Bhimavaram"}
            };
        }
    }
}