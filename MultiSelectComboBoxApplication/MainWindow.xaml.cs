﻿using MultiSelectComboBoxApplication.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MultiSelectComboBoxApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Items_Checked(object sender, RoutedEventArgs e)
        {
            lstSelectedItems.Items.Clear();
            foreach (Team team in ddlTeam.ItemsSource)
            {
                if (team.Id == 1 && team.CheckStatus)
                {
                    List<Team> Teams = new List<Team>();
                    foreach (Team teamData in ddlTeam.ItemsSource)
                    {
                        teamData.CheckStatus = true;
                        Teams.Add(teamData);
                    }
                    //lstSelectedItems.Items.Add(team.Name);
                    ddlTeam.ItemsSource = Teams;
                }
                //else if (team.Id == 1 && !team.CheckStatus)
                //{
                //    List<Team> Teams = new List<Team>();
                //    foreach (Team teamData in ddlTeam.ItemsSource)
                //    {
                //        if (teamData.Id == 1) { teamData.CheckStatus = false; }
                //        Teams.Add(teamData);
                //    }
                //    //lstSelectedItems.Items.Add(team.Name);
                //    ddlTeam.ItemsSource = Teams;
                //}
                else if (team.CheckStatus)
                {
                    lstSelectedItems.Items.Add(team.Name);
                }
            }
        }

        private void Items_Unchecked(object sender, RoutedEventArgs e)
        {
            lstSelectedItems.Items.Clear();
            bool selectAllcheckStatus = false;
            foreach (Team team in ddlTeam.ItemsSource)
            {
                if (team.CheckStatus && team.Id != 1)
                {
                    lstSelectedItems.Items.Add(team.Name);
                }
                if (team.Id == 1)
                {
                    selectAllcheckStatus = team.CheckStatus;
                }
            }
            if (lstSelectedItems.Items.Count < 3)
            {
                List<Team> Teams = new List<Team>();
                foreach (Team teamData in ddlTeam.ItemsSource)
                {
                    if (teamData.Id == 1)
                    {
                        teamData.CheckStatus = false;
                    }
                    Teams.Add(teamData);
                }
                ddlTeam.ItemsSource = Teams;
            }
            if (lstSelectedItems.Items.Count == 3 && selectAllcheckStatus == false)
            {
                List<Team> Teams = new List<Team>();
                foreach (Team teamData in ddlTeam.ItemsSource)
                {
                    teamData.CheckStatus = false;
                    Teams.Add(teamData);
                }
                ddlTeam.ItemsSource = Teams;
                lstSelectedItems.Items.Clear();
            }
        }
    }
}
