﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiSelectComboBoxApplication.Model
{
    public class Team
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Boolean CheckStatus { get; set; }
    }
}
