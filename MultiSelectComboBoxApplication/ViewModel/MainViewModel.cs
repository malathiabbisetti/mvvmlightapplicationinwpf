using GalaSoft.MvvmLight;
using MultiSelectComboBoxApplication.Model;
using System.Collections.Generic;
using System.Windows;

namespace MultiSelectComboBoxApplication.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        public string Title { get; set; }

        public List<Team> Teams { get; } 

        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                Title = "Multi Select ComboBox Application(Design Mode)";
            }
            else
            {
                // Code runs "for real"
                Title = "Multi Select ComboBox Application";
            }

            Teams = new List<Team>()
            {
                new Team(){Id = 1, Name = "Select All"},
                new Team(){Id = 2, Name = "DotNet"},
                new Team(){Id = 3, Name = "Java"},
                new Team(){Id = 4, Name = "Js"}
            };
           
        }
    }
}