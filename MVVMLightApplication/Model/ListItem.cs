﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVMLightApplication.Model
{
    public class ListItem
    {
        public ListItem(string name)
        {
            Name = name;
        }

        public string Name { get; set; }
    }
}
