using GalaSoft.MvvmLight;
using MVVMLightApplication.Model;
using System.Collections.Generic;

namespace MVVMLightApplication.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            if (IsInDesignMode)
            {
                // Code runs in Blend --> create design time data.
                Title = "Hello MVVM Light (Design Mode)";
            }
            else
            {
                // Code runs "for real"
                Title = "Hello MVVM Light";
            }

            Items.Add(new ListItem("Text 1"));
            Items.Add(new ListItem("Text 2"));
            Items.Add(new ListItem("Text 3"));
        }

        public string Title { get; set; }
        public List<ListItem> Items { get; } = new List<ListItem>();
    }
}